using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApi.Controllers
{
    [Route("/test")]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get([FromJsonQuery]Item item)
        {
            return new JsonResult(item);
        }
    }
}