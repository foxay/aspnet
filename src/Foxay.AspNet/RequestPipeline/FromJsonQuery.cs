using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.AspNet.RequestPipeline
{
    public class FromJsonQuery : FromFormatedStringQuery
    {
        public FromJsonQuery() : base(typeof(JsonBinder))
        {
        }
    }
}