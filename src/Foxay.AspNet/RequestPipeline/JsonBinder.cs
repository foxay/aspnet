using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Foxay.AspNet.RequestPipeline
{
    public class JsonBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }
            
            string name = bindingContext.OriginalModelName;
            string json = bindingContext
                .ActionContext
                .HttpContext
                .Request
                .Query.FirstOrDefault(x => x.Key == name).Value;
            Type modelType = bindingContext.ModelType;
            object? model = Serializer.WithSettings((settings)=>
                {  
                }).Deserialize(modelType, json, SerializationFormat.JSON);
            bindingContext.Result = ModelBindingResult.Success(model);
            return Task.CompletedTask;
        }
    }
}