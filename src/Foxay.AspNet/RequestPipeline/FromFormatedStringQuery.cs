using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Foxay.AspNet.RequestPipeline
{
    public class FromFormatedStringQuery: ModelBinderAttribute
    {
        public FromFormatedStringQuery(Type binderType) : base(binderType)
        {
        }
    }
}